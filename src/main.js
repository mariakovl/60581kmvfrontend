import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "axios";
import PrimeVue from "primevue/config";
import "primevue/resources/themes/bootstrap4-light-blue/theme.css";
import "primeicons/primeicons.css";
import "primeflex/primeflex.css";
import "primevue/resources/primevue.min.css";
import ConfirmationService from 'primevue/confirmationservice';
import Tooltip from 'primevue/tooltip';

const myApp = createApp(App);
window.axios = axios;
myApp.use(store)
myApp.use(router)
myApp.use(PrimeVue);
myApp.use(ConfirmationService);
myApp.directive('tooltip', Tooltip);
myApp.mount('#app');