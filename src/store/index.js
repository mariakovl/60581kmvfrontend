import { createStore } from "vuex";
import axios from "axios";
import router from "../router/index";

const backendUrl = process.env.VUE_APP_BACKEND_URL;
const client_id = process.env.VUE_APP_CLIENT_ID;
const client_secret = process.env.VUE_APP_CLIENT_SECRET;

export default createStore({
  state: {
    user: {},
    token: null,
    preLoading: false,
    dataPreLoading: true,
    loginError: false,
    loggedIn: false,
    recipe: {},
    countRecipe: 0,
    pager: {
      currentPage: 1,
      perPage: 5,
    },
    categories: {},
    validation: {},
    createRecipeDialogVisible: false,
  },
  mutations: {
    setToken(state, token) {
      state.token = token;
    },
    getToken(context, token) {
      context.token = token;
    },
    setUser(context, user) {
      context.user = user;
    },
    setPreloading(context, is_load) {
      context.preLoading = is_load;
    },
    setDataPreloading(context, is_load) {
      context.dataPreLoading = is_load;
    },
    setLoginError(context, isError) {
      context.loginError = isError;
    },
    setLoggedIn(context, isLoggedIn) {
      context.loggedIn = isLoggedIn;
    },
    setRecipe(context, recipe) {
      context.recipe = recipe;
    },
    setCategories(context, categories){
      context.categories = categories;
    },
    setPage(context, page) {
      context.pager.currentPage = page;
    },
    setPager(context, pager) {
      context.pager = pager;
    },
    setRecipeCount(context, count) {
      context.countRecipe = parseInt(count);
    },
    setValidation(context, validation) {
      context.validation = validation;
    },
    setCreateRecipeDialogVisible(context, visible) {
      context.createRecipeDialogVisible = visible;
    },
    logout(context) {
      context.user = null;
      context.token = null;
      context.loggedIn = false;
      context.recipe = {};
      localStorage.removeItem("token");
      router.push("/");
    },
  },
  actions: {
    auth(context, { login, password }) {
      context.commit("setPreloading", true);
      axios
        .post(
          backendUrl + "/OAuthController/Authorize",
          {
            username: login,
            password: password,
            grant_type: "password",
          },
          {
            headers: {
              "Content-Type": "application/json;charset=UTF-8",
              "Access-Control-Allow-Origin": "*",
              Authorization:
                "Basic " + window.btoa(client_id + ":" + client_secret),
            },
          }
        )
        .then((response) => {
          if (response.data.access_token) {
            context.commit("setToken", response.data.access_token);
            localStorage.setItem("token", response.data.access_token);
            context.commit("setLoggedIn", true);
            context.commit("setLoginError", false);
            context.dispatch("getUser");
          } else {
            context.commit("setLoginError", true);
            context.commit("setPreloading", false);
          }
        });
    },
    getUser(context) {
      //context.commit("setPreloading", true);
      console.log("get user");
      return window.axios
        .get(backendUrl + "/OAuthController/user", {
          headers: {
            "Access-Control-Allow-Origin": "*",
            Authorization: "Bearer " + context.state.token,
          },
        })
        .then((response) => {
          console.log(response.data);
          context.commit("setUser", response.data);
          context.commit("setPreloading", false);
        })
        .catch((error) => {
          if (error.response) context.commit("setLoggedIn", false);
        });
    },
    getRecipe(context, { perPage, currentPage, search }) {
      context.commit("setDataPreloading", true);
      const params = new URLSearchParams();
      params.append("per_page", perPage);
      params.append("current_page", currentPage);
      params.append("search", search);
      return window.axios
        .post(backendUrl + "/RecipeApi/findRecipe", params, {
          headers: {
            "Access-Control-Allow-Origin": "*",
            Authorization: "Bearer " + context.state.token,
          },
        })
        .then((response) => {
          console.log("response", response.data);
          context.commit("setRecipe", response.data.dish);
          context.commit("setDataPreloading", false);
        });
    },

    loadCategories(context) {
      context.commit("setDataPreloading", true);
      return window.axios
        .post(backendUrl + "/RecipeApi/categories", null, {
          headers: {
            "Access-Control-Allow-Origin": "*",
            Authorization: "Bearer " + context.state.token,
          },
        })
        .then((response) => {
          context.commit("setCategories", response.data.categories);
          context.commit("setDataPreloading", false);
        });
    },

    getRecipeCount(context, search = "") {
      context.commit("setDataPreloading", true);
      const params = new URLSearchParams();
      params.append("search", search);
      return window.axios
        .post(backendUrl + "/RecipeApi/recipeCount", params)
        .then((response) => {
          context.commit("setRecipeCount", response.data.count.count);
          context.commit("setDataPreloading", false);
          console.log("data", response.data.count.count);
        });
    },
    createRecipe(
      context,
      { name, id_category, time, cookingmethod, picture}
    ) {
      console.log("create rating");
      context.commit("setDataPreloading", true);

      let formData = new FormData();
      formData.append('name', name);
      formData.append('id_category', id_category);
      formData.append('time', time);
      formData.append('cookingmethod', cookingmethod);
      formData.append('picture', picture);

      return window.axios
        .post(backendUrl + "/RecipeApi/store", formData, {
          headers: {
            Authorization: "Bearer " + context.state.token,
          },
        })
        .then((response) => {
          if (response.status === 201) {
            console.log("Rating created successfully");
            context.commit("setValidation", {});
            context.commit("setCreateRecipeDialogVisible", false);
            context.commit("setPage", context.state.pager.pageCount);
            this.dispatch("getRecipe");
            router.push("/recipe");
          } else {
            console.log(response.data);
            context.commit("setValidation", response.data);
          }
        });
    },
    deleteRecipe(context, { id }) {
      console.log("delete dish");
      context.commit("setDataPreloading", true);
      return window.axios
        .get(backendUrl + "/RecipeApi/delete/" + id, {
          headers: {
            Authorization: "Bearer " + context.state.token,
          },
        })
        .then((response) => {
          if (response.status === 200) {
            console.log("Dish deleted successfully");
            context.commit("setPage", context.state.pager.pageCount);
            this.dispatch("getRecipe");
            router.push("/recipe");
          } else {
            console.log(response.data);
          }
        });
    },
  },
});
