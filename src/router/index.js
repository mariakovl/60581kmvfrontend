import { createRouter, createWebHistory } from "vue-router";
import Homepage from "../components/Homepage";
import Recipe from "../components/Recipe";
import LoginDialog from "../components/LoginDialog";

const routes = [
  {
    path: "/",
    component: Homepage,
  },
  {
    path: "/recipe",
    component: Recipe,
  },
  {
    path: "/login",
    component: LoginDialog,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
